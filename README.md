# Noticias Leganés - iOS
## Author: Álvaro Blázquez
### Description: App to show Leganes's news, the matches and league table

### Dependencies::
- FeedKit
- Kingfisher
- TaskQueue
- SwiftyJSON
- Alamofire
- Fabric
- Crashlytics
- SwiftHEXColors
