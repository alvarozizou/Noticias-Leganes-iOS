//
//  String+HTML.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 18/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

extension String {
    
    public func removingHTMLOccurrences() -> String{
        
        var result = self
        
        result = result.replacingOccurrences(of: "&lt;strong&gt;", with: "")
        result = result.replacingOccurrences(of: "&lt;/strong&gt;", with: "")
        result = result.replacingOccurrences(of: "&quot;", with: "")
        
        return result
        
    }
    
    public func addEllipsis() -> String {
        guard let lastChar = self.last else {
            return self
        }
    
        return lastChar != "." ? self + "..." : self
    }
    
}
