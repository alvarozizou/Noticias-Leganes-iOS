//
//  Utils.swift
//  NoticiasLeganes
//
//  Created by Alvaro Informática on 12/1/18.
//  Copyright © 2018 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

/// Static utils functions for the project
struct Utils {
    /// Static variables of the device
    struct Device {
        static var uuid: String {
            return UUID().uuidString
        }
        static var model: String {
            return UIDevice.current.model
        }
        static var vso: String {
            return UIDevice.current.systemVersion
        }
        static var vapp: String {
            guard let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
                return ""
            }
            return version
        }
        static var language: String {
            let lang = Locale.preferredLanguages[0]
            let langDic = Locale.components(fromIdentifier: lang)
            let localeLang = langDic["kCFLocaleLanguageCodeKey"] ?? "es"
            return localeLang == "pt" ? "pt" : "es"
        }
    }

    /// Is a Valid Email
    static func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
}


struct UserDF {
    enum Key: String {
        case alias
    }

    static var alias: String? {
        get {
            let defaults = UserDefaults.standard
            return defaults.string(forKey: UserDF.Key.alias.rawValue)
        }
        set {
            let defaults = UserDefaults.standard
            defaults.set(newValue, forKey: UserDF.Key.alias.rawValue)
        }
    }
}
