//
//  UIImageView+KingFisher.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 18/6/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    public func setPhotoImageKf(withImage entryImage : Any?,
                                placeholder : UIImage?,
                                processor : RoundCornerImageProcessor?) {
        
        if let image = entryImage as? URL {
            
            var options : KingfisherOptionsInfo?
            if let _processor = processor {
                options = [.processor(_processor)]
            }
            
            self.kf.setImage(with: image, placeholder: placeholder, options: options)
            
        } else if let image = entryImage as? UIImage {
            self.image = image
        }
        
    }
    
    

}
