//
//  RSSFeedItem + Entry.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 15/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import FeedKit

extension RSSFeedItem {
    
    private func toNews(feed : Feed) -> News? {
        
        guard let title = self.title else {
            return nil
        }
        let description = getDescription(feed: feed)
        
        let imageUrl = !feed.isMedia ? self.enclosure?.attributes?.url : self.media?.mediaBackLinks?.first
        
        let defaultImage = UIImage(named: feed.defaultImage)
        let origen = feed.origen
        
        let news = News(title : title, link : self.link, date : self.pubDate, imageUrl : imageUrl, description : description, origen : origen, defaultImage : defaultImage)
        
        return news
    }
    
    private func getDescription(feed: Feed ) -> String? {
        
        if (feed.hasDesc){
            
            return !feed.isMedia ? self.description : self.media?.mediaDescription?.value
            
        } else if (feed.hasMeta) {
            
            return self.content?.contentEncoded
            
        }
        
        return nil
    }
    
    static public func downloadNews(feed : Feed) -> [News] {
        
        var entries = [News]()
        //Solución provisional hasta que arreglen el bug de FeedParser cuando no hay conexión
        if  let url  = feed.url,
            let data = NSData(contentsOf: url) as Data? {
            
            let result = FeedParser(data: data).parse()
            
            guard let resultFeed = result.rssFeed, result.isSuccess else { return entries }
            
            if let items = resultFeed.items {
                for item in items {
                    if let entry = item.toNews(feed : feed) {
                        entries.append(entry)
                    }
                }
            }
        }
        
        return entries
        
    }
    

}
