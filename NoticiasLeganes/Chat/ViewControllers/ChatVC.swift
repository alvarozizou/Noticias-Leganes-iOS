//
//  ChatVC.swift
//  NoticiasLeganes
//
//  Created by Alvaro Informática on 16/1/18.
//  Copyright © 2018 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ChatVC: UIViewController {

    /*var viewModel: ChatVM!

    var activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)

    let chat = "Chat"
    let channel = "General"
    let typing = "GeneralTyping"

    private lazy var messageRef: DatabaseReference = Database.database().reference().child(chat).child(channel)
    private var newMessageRefHandle: DatabaseHandle?
    var messages = [JSQMessage]()

    private lazy var userIsTypingRef: DatabaseReference = Database.database().reference().child(chat).child(typing).child(self.senderId)
    private var localTyping = false
    var isTyping: Bool {
        get {
            return localTyping
        }
        set {
            localTyping = newValue
            userIsTypingRef.setValue(newValue)
        }
    }
    private lazy var usersTypingQuery: DatabaseQuery =
        Database.database().reference().child(chat).child(typing).queryOrderedByValue().queryEqual(toValue: true)

    lazy var outgoingBubbleImageView: JSQMessagesBubbleImage = self.setupOutgoingBubble()
    lazy var incomingBubbleImageView: JSQMessagesBubbleImage = self.setupIncomingBubble()

    var firstTime = true

    override func viewDidLoad() {
        super.viewDidLoad()
        //navigationController?.isNavigationBarHidden = true
        edgesForExtendedLayout = []
        if viewModel.isRegister {
            senderId = viewModel.uid
            senderDisplayName = viewModel.alias
            inputToolbar.contentView.rightBarButtonItem.setTitle("ENVIAR", for: .normal)
            inputToolbar.contentView.rightBarButtonItemWidth = 60.0
        } else {
            senderId = ""
            senderDisplayName = ""
            inputToolbar.contentView.rightBarButtonItem.setTitle("¡Conecta!", for: .normal)
            inputToolbar.contentView.rightBarButtonItem.isEnabled = true
            inputToolbar.contentView.rightBarButtonItemWidth = 150.0
            inputToolbar.contentView.textView.isUserInteractionEnabled = false
            inputToolbar.contentView.leftBarButtonItem.isUserInteractionEnabled = false
        }

        observeMessages()
        prepareHideKeyboard()

        // No avatars
        collectionView!.collectionViewLayout.incomingAvatarViewSize = CGSize.zero
        collectionView!.collectionViewLayout.outgoingAvatarViewSize = CGSize.zero

        inputToolbar.contentView.leftBarButtonItem.isHidden = true
        inputToolbar.contentView.leftBarButtonItemWidth = 0.0

        Analytics.watchChat()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if firstTime {
            prepareActivityIndicator()
            firstTime = false
        }
        if viewModel.isRegister &&
            !inputToolbar.contentView.textView.isUserInteractionEnabled {
            senderId = viewModel.uid
            senderDisplayName = viewModel.alias
            inputToolbar.contentView.rightBarButtonItem.setTitle("ENVIAR", for: .normal)
            inputToolbar.contentView.rightBarButtonItem.isEnabled = false
            inputToolbar.contentView.rightBarButtonItemWidth = 60.0
            inputToolbar.contentView.textView.isUserInteractionEnabled = true
        }
        if viewModel.isRegister {
            observeTyping()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    private func addMessage(withId id: String, name: String, text: String, date: Date) {
        if let message = JSQMessage(senderId: id, senderDisplayName: name, date: date, text: text) {
            messages.append(message)
        }
    }

    private func observeMessages() {
        //messageRef = Database.database().reference().child(chat).child(channel)
        let messageQuery = messageRef.queryLimited(toLast: 500)

        newMessageRefHandle = messageQuery.observe(.childAdded, with: { (snapshot) -> Void in
            self.activityIndicator.stopAnimating()
            let messageData = snapshot.value as! Dictionary<String, String>
            if let id = messageData["senderId"] as String!,
                let name = messageData["senderName"] as String!,
                let text = messageData["text"] as String!,
                let date = messageData["date"]?.toDate(dateFormat: "yyyy-MM-dd HH:mm"),
                text.count > 0 {
                self.addMessage(withId: id, name: name, text: text, date: date)
                self.finishReceivingMessage()
            }
        })
    }

    private func observeTyping() {
        let typingIndicatorRef = Database.database().reference().child(chat).child(typing)
        userIsTypingRef = typingIndicatorRef.child(senderId)
        userIsTypingRef.onDisconnectRemoveValue()

        usersTypingQuery.observe(.value) { (data: DataSnapshot) in
            if data.childrenCount == 1 && self.isTyping {
                return
            }
            self.showTypingIndicator = data.childrenCount > 0
            self.scrollToBottom(animated: true)
        }
    }*/

}
/*
extension ChatVC {
    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageDataForItemAt indexPath: IndexPath!) -> JSQMessageData! {
        return messages[indexPath.item]
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, messageBubbleImageDataForItemAt indexPath: IndexPath!) -> JSQMessageBubbleImageDataSource! {
        let message = messages[indexPath.item]
        if message.senderId == senderId {
            return outgoingBubbleImageView
        } else {
            return incomingBubbleImageView
        }
    }



    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        let message = messages[indexPath.item]

        if message.senderId == senderId {
            cell.textView?.textColor = UIColor.white
        } else {
            cell.textView?.textColor = UIColor.black
        }
        return cell
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, avatarImageDataForItemAt indexPath: IndexPath!) -> JSQMessageAvatarImageDataSource! {
        return nil
    }

    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        if senderId == "" {
            performSegue(withIdentifier: "Chat2Alias", sender: self)
            return
        }
        let itemRef = messageRef.childByAutoId()
        let messageItem = [
            "senderId": senderId!,
            "senderName": senderDisplayName!,
            "text": text!,
            "date": Date().toString(dateFormat: "yyyy-MM-dd HH:mm")
            ]

        itemRef.setValue(messageItem)
        isTyping = false

        finishSendingMessage()
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath) -> NSAttributedString? {
        let message = messages[indexPath.item]
        return message.senderId == senderId ? nil :
                NSAttributedString(string: message.senderDisplayName + " - " + message.date.toString(dateFormat: "HH:mm"))
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout, heightForMessageBubbleTopLabelAt indexPath: IndexPath) -> CGFloat {
        let message = messages[indexPath.item]
        return message.senderId == senderId ? 0.0 : kJSQMessagesCollectionViewAvatarSizeDefault
    }

    private func setupOutgoingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.outgoingMessagesBubbleImage(with: UIColor.FlatColor.Blue.Leganes)
    }

    private func setupIncomingBubble() -> JSQMessagesBubbleImage {
        let bubbleImageFactory = JSQMessagesBubbleImageFactory()
        return bubbleImageFactory!.incomingMessagesBubbleImage(with: UIColor.jsq_messageBubbleLightGray())
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapCellAt indexPath: IndexPath!, touchLocation: CGPoint) {
        view.endEditing(true)
    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView!, didTapMessageBubbleAt indexPath: IndexPath!) {
        view.endEditing(true)
    }

}

extension ChatVC {
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        isTyping = textView.text != ""
    }
}

extension ChatVC {

    func prepareHideKeyboard() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        collectionView.addGestureRecognizer(tap)
    }

    @objc func hideKeyboard() {
        if inputToolbar.contentView.textView.isFirstResponder {
            inputToolbar.contentView.textView.resignFirstResponder()
        }
    }

    func prepareActivityIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.color = K.mainColor
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Chat2Alias",
            let viewController = segue.destination as? AliasVC {
            viewController.viewModel = viewModel
        }
    }
}*/
