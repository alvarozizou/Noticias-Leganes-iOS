//
//  AliasVC.swift
//  NoticiasLeganes
//
//  Created by Alvaro Informática on 16/1/18.
//  Copyright © 2018 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Material
import Firebase

class AliasVC: InputDataViewController {

    @IBOutlet weak var aliasView: UIView!
    private var aliasField: ErrorTextField!

    var viewModel: ChatVM!

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareAliasField()
        Analytics.watchAliasChat()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func initChatTap(_ sender: Any) {
        guard let textAlias = aliasField.text else {
                return
        }
        if textAlias == "" || textAlias.count < 5 {
            aliasField.isErrorRevealed = true
            return
        }

        Auth.auth().signInAnonymously(completion: { (user, error) in
            if let _error = error {
                print(_error.localizedDescription)
                return
            }
            UserDF.alias = textAlias
            self.viewModel.alias = textAlias
            self.viewModel.uid = Auth.auth().currentUser?.uid
            self.navigationController?.popViewController(animated: true)
        })
    }

}

extension AliasVC {
    private func prepareAliasField() {
        aliasField = prepareErrorTextField(placeholder: "Alias", error: "Debe tener al menos 5 caracteres", centerInLayout: aliasView)
    }
}
