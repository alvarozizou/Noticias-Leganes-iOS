//
//  SignUpVC.swift
//  NoticiasLeganes
//
//  Created by Alvaro Informática on 12/1/18.
//  Copyright © 2018 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Material
import Firebase

class SignUpVC: InputDataViewController {

    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var passwordView: UIView!

    private var emailField: ErrorTextField!
    private var passwordField: ErrorTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        prepareEmailField()
        preparePasswordField()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func initChatTap(_ sender: Any) {
        guard let textEmail = emailField.text,
            let textPassword = passwordField.text else {
            return
        }
        if textEmail == "" || !Utils.isValidEmail(testStr: textEmail) {
            emailField.isErrorRevealed = true
            return
        }
        if textPassword == "" || textPassword.count < 5 {
            passwordField.isErrorRevealed = true
            return
        }

        Auth.auth().createUser(withEmail: textEmail, password: textPassword, completion: { (user, error) in

            if let _error = error {
                if let errCode = AuthErrorCode(rawValue: _error._code) {
                    switch errCode {
                    case .emailAlreadyInUse:
                        self.logIn(withEmail: textEmail, password: textPassword)
                    default:
                        print("others")
                    }
                }
            }
        })
    }

    func logIn(withEmail email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
            if let _error = error {
                if let errCode = AuthErrorCode(rawValue: _error._code) {
                    switch errCode {
                    case .wrongPassword:
                        print("Aqui")
                    default:
                        print(_error)
                    }
                }
            }
        })
    }
}

extension SignUpVC {
    private func prepareEmailField() {
        emailField = prepareErrorTextField(placeholder: "Email", error: "Email incorrecto", centerInLayout: emailView)
        emailField.keyboardType = .emailAddress
    }

    private func preparePasswordField() {
        passwordField = prepareErrorTextField(placeholder: "Contraseña", error: "Debe tener al menos 5 caracteres", centerInLayout: passwordView)

        passwordField.isVisibilityIconButtonEnabled = true
        passwordField.visibilityIconButton?.tintColor = UIColor.FlatColor.Blue.Leganes.withAlphaComponent(passwordField.isSecureTextEntry ? 0.38 : 0.54)
    }
}
