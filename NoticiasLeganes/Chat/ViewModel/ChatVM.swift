//
//  ChatVM.swift
//  NoticiasLeganes
//
//  Created by Alvaro Informática on 23/1/18.
//  Copyright © 2018 Alvaro Blazquez Montero. All rights reserved.
//
import Firebase

class ChatVM {
    var uid: String?
    var alias: String?

    var isRegister: Bool {
        return alias != nil && uid != nil
    }

    init() {
        if let uid = Auth.auth().currentUser?.uid,
            let alias = UserDF.alias {
            self.uid = uid
            self.alias = alias
        }
    }
}
