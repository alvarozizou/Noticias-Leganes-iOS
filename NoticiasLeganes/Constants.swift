//
//  Constants.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 03/8/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit
import SwiftHEXColors

struct K {

    private init() {}
    
    private struct ApiFootball {
        static let Url      = "http://api.football-data.org/v2"
        
        static let IdTeam   = "745"
        static let IdLeague = "2014"
        
        static let Match    = "/teams/" + ApiFootball.IdTeam + "/matches/"
        static let Position = "/competitions/" + ApiFootball.IdLeague + "/standings"
    }

    public static let Key         = "297271fd514843ddaa3147f75a8fc12b"
    public static let MatchURL    = ApiFootball.Url + ApiFootball.Match
    public static let PositionURL = ApiFootball.Url + ApiFootball.Position
    public static let nameTeam    = "CD Leganés"
    
    public static let mainColor = UIColor(hexString: "#3F51B5")
    public static let mainColorAlpha = UIColor(hexString: "#3F51B5", alpha: 0.2)
    
    public struct Segues {
        public static let news2entry = "news2entry"
    }
    
}
