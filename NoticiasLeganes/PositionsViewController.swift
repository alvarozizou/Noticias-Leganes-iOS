//
//  PositionsViewController.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 25/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

class PositionsViewController: UIViewController, ListViewController {

    //MARK: Properties
    @IBOutlet weak var tableView: UITableView!
    var activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    var refreshControl    = UIRefreshControl()
    
    var viewModel : ViewModel<PositionItemViewModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        Analytics.watchPositions()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareTableView() {
        prepareRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refreshData(_ sender: Any) {
        reloadData()
    }
    
    func reloadData() {
        viewModel.reloadData()
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }

}
    
 extension PositionsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PositionTableViewCell.ReuseIdentifier, for: indexPath) as? PositionTableViewCell  else {
            fatalError("The dequeued cell is not an instance of PositionTableViewCell.")
        }
        
        if let positionItemViewModel = viewModel.itemAtIndex(indexPath.row) {
            cell.positionItemViewModel = positionItemViewModel
        }
        
        return cell
    }
}

extension PositionsViewController: ListViewModelDelegate {
    
    func itemsDidChange() {
        showNewData()
    }
    
    func itemsError() {
        showError()
    }
}
