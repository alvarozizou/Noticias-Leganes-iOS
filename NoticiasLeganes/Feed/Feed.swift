//
//  Feed.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 28/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit
import FeedKit

import SwiftyJSON

private enum FeedKey: String {
    case url, origen, defaultImage, isMedia, hasDesc, hasMeta, feeds
}

public struct Feed {

    let url          : URL?
    let origen       : String
    let isMedia      : Bool
    let hasDesc      : Bool
    let hasMeta      : Bool
    let defaultImage : String

    var news      : [News]?
    
    init(json: JSON) {
        
        
        self.url          = URL(string: json[FeedKey.url.rawValue].stringValue)
        self.origen       = json[FeedKey.origen.rawValue].stringValue
        self.isMedia      = json[FeedKey.isMedia.rawValue].boolValue
        self.hasDesc      = json[FeedKey.hasDesc.rawValue].boolValue
        self.hasMeta      = json[FeedKey.hasMeta.rawValue].boolValue
        self.defaultImage = json[FeedKey.defaultImage.rawValue].stringValue
        
    }
    
    mutating func downloadNews() {
        self.news = RSSFeedItem.downloadNews(feed: self)
    }
    
}


extension Feed  {
    
    private static let resourceFeed      = "feeds"
    private static let extensionResource = "json"
    typealias completion = (Any?, Error?) -> ()
    
    private enum FeedError: Error {
        case notFound
    }
    
    static func getData(completionHandler: @escaping completion) {
        
        guard let file = Bundle.main.url(forResource: resourceFeed, withExtension: extensionResource),
            let data = try? Data(contentsOf: file) else {
                completionHandler(nil, FeedError.notFound)
                return
        }
        
        let json = try! JSON(data: data)
        let feeds = getList(json: json)
        completionHandler(feeds, nil)
    }
    
    private static func getList(json: JSON) -> [Feed] {
        var feeds = [Feed]()
        
        let feedsJson: Array<JSON> = json[FeedKey.feeds.rawValue].arrayValue
        for feedJson in feedsJson {
            feeds.append( Feed(json: feedJson) )
        }
        
        return feeds
    }
    
}
