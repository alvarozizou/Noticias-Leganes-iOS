//
//  PositionItemViewModel.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 23/7/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit

protocol PositionItemViewModel {
    var teamName:     String {get}
    var positionTeam: String {get}
    var points:       String {get}
    var playedGames:  String {get}
    var wins:         String {get}
    var draws:        String {get}
    var losses:       String {get}
    var imageUrl:     URL? {get}
    var isTeamApp:    Bool {get}
}

struct PositionItemViewModelImpl: PositionItemViewModel {
    
    var position : Position
    
    var teamName : String {
        return self.position.teamName
    }
    
    var positionTeam : String {
        return String(self.position.position)
    }
    
    var points : String {
        return String(self.position.points)
    }
    
    var playedGames : String {
        return String(self.position.playedGames)
    }
    
    var wins : String {
        return String(self.position.wins)
    }
    
    var draws : String {
        return String(self.position.draws)
    }
    
    var losses : String {
        return String(self.position.losses)
    }
    
    var imageUrl : URL? {
        return URL(string: self.position.crestURI)
    }
    
    var isTeamApp: Bool {
        return self.position.teamName == K.nameTeam
    }
    
    init(_ position: Position) {
        self.position = position
    }
    
}
