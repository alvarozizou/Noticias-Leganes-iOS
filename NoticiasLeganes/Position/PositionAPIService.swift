//
//  PositionAPIService.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 17/9/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit
import Alamofire
import SwiftyJSON

class PositionAPIService: DataManager {
    
    func getData(completionHandler: @escaping completion) {
		let headers: HTTPHeaders = [
            		"X-Auth-Token": K.Key
        	]
        
		Alamofire.request(K.PositionURL, method: .get, headers: headers).validate().responseJSON { response in
		    switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let positions = self.getList(json: json)
                    completionHandler(positions, nil)
                case .failure(let error):
                    completionHandler(nil, error)
		    }
		}
	}
	
	private func getList(json: JSON) -> [Position] {
		guard let standings = json[PositionKey.standing.rawValue]
                                .arrayValue
                                .first(where:
                                    { $0[PositionKey.type.rawValue].stringValue == PositionKey.total.rawValue }
                                )
                                else {
            return [Position]()
        }
        return standings[PositionKey.table.rawValue].arrayValue.map { Position(json: $0) }
	}
}
