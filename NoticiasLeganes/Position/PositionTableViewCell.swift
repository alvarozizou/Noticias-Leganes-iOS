//
//  PositionTableViewCell.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 23/7/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit

class PositionTableViewCell: UITableViewCell {
    
    static let ReuseIdentifier = "PositionCell"

    @IBOutlet weak var positionLabel:    UILabel!
    @IBOutlet weak var teamNameLabel:    UILabel!
    @IBOutlet weak var pointsLabel:      UILabel!
    @IBOutlet weak var playedGamesLabel: UILabel!
    @IBOutlet weak var winsLabel:        UILabel!
    @IBOutlet weak var drawsLabel:       UILabel!
    @IBOutlet weak var lossesLabel:      UILabel!
    //@IBOutlet weak var logoImageView:    UIImageView!
    
    var positionItemViewModel: PositionItemViewModel? {
        didSet {
            bindViewModel()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func bindViewModel() {
		if let _positionItemViewModel = positionItemViewModel {
        	self.positionLabel.text    = _positionItemViewModel.positionTeam
        	self.teamNameLabel.text    = _positionItemViewModel.teamName
        	self.pointsLabel.text      = _positionItemViewModel.points
        	self.playedGamesLabel.text = _positionItemViewModel.playedGames
        	self.winsLabel.text        = _positionItemViewModel.wins
        	self.drawsLabel.text       = _positionItemViewModel.draws
        	self.lossesLabel.text      = _positionItemViewModel.losses
            self.backgroundColor       = _positionItemViewModel.isTeamApp ? K.mainColorAlpha : .white
        	//self.logoImageView.setPhotoImageKf(withImage: position.imageUrl, placeholder: nil, processor: nil)
		}
    }

}
