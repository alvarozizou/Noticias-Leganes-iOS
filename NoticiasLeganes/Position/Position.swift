//
//  Position.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 16/6/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit
import SwiftyJSON

enum PositionKey: String {
    case position, team, name , crestURI, points, playedGames, wins, draws, losses, standing = "standings"
    case type, table, total = "TOTAL"
}

struct Position {
    var position    : Int     = 0
    var teamName    : String  = ""
    var crestURI    : String  = ""
    var points      : Int     = 0
    var playedGames : Int     = 0
    var wins        : Int     = 0
    var draws       : Int     = 0
    var losses      : Int     = 0
}


extension Position {
	init (json: JSON) {
        self.init()
		self.position    = json[PositionKey.position.rawValue].intValue
		self.teamName    = json[PositionKey.team.rawValue][PositionKey.name.rawValue].stringValue
		self.crestURI    = json[PositionKey.crestURI.rawValue].stringValue
		self.points      = json[PositionKey.points.rawValue].intValue
		self.playedGames = json[PositionKey.playedGames.rawValue].intValue
		self.wins        = json[PositionKey.wins.rawValue].intValue
		self.draws       = json[PositionKey.draws.rawValue].intValue
		self.losses      = json[PositionKey.losses.rawValue].intValue
	}
}
