//
//  EntryViewController.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 26/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

class NewsViewController: UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noticiaButton: UIButton!
    private let placeholderNamed = "ic_launcher_bn"
    
    var newsItemViewModel : NewsItemViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareView()
        Analytics.watchNews(newsItemViewModel)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func prepareView() {
        
        self.titleLabel.text = newsItemViewModel.title
        self.descTextView.text = newsItemViewModel.desc
        self.dateLabel.text = newsItemViewModel.dateFormatted
        
        let placeholder = UIImage(named: placeholderNamed)
        //TODO: Hacerlo en todos los idiomas y coger el titulo desde un string
        self.noticiaButton.setTitle((self.noticiaButton.titleLabel?.text)! + " en " + newsItemViewModel.origen,
                                    for: UIControl.State.normal)
        photoImage.setPhotoImageKf(withImage: newsItemViewModel.image,
                                   placeholder: placeholder,
                                   processor: nil)
    }
    
    @IBAction func readNew(_ sender: Any) {
        
        guard let urlString = self.newsItemViewModel.link?.absoluteString,
              let url       = URL(string: urlString) 
              else { return }
        
        Analytics.watchOriginalNews(newsItemViewModel)
        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        
    }
    
    
    @IBAction func share(_ sender: Any) {
        
        guard let urlString = self.newsItemViewModel.link?.absoluteString else {
            return
        }
        
        Analytics.shareNews(newsItemViewModel)
        let sharedText = "App: Noticias Leganes, Twitter: @ThisIsButarque"
        let finalText = urlString + " " + sharedText
        // set up activity view controller
        let textToShare = [ finalText ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        //activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
        
    }

}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
