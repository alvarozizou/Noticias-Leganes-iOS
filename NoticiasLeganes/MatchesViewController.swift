//
//  ScheduleViewController.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 25/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

class MatchesViewController: UIViewController, ListViewController {

    @IBOutlet weak var tableView: UITableView!
    var activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    var refreshControl    = UIRefreshControl()
    var firstTime = true
    
    var viewModel : ViewModel<MatchItemViewModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        Analytics.watchMatches()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareTableView() {
        prepareRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refreshData(_ sender: Any) {
        reloadData()
    }
    
    func reloadData() {
        viewModel.reloadData()
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }
}
    
extension MatchesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: MatchTableViewCell.ReuseIdentifier, for: indexPath) as? MatchTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MatchTableViewCell.")
        }
        if let matchItemViewModel = viewModel.itemAtIndex(indexPath.row) {
            cell.matchItemViewModel = matchItemViewModel
        }
        
        return cell
    }
}

extension MatchesViewController: ListViewModelDelegate {
    
    func itemsDidChange() {
        showNewData()
        if firstTime {
            let index = IndexPath(row: findFirstOpenMatch(), section: 0)
            tableView.scrollToRow(at: index,at: .middle, animated: true)
            firstTime = false
        }
    }
    
    func itemsError() {
        showError()
    }
    
    func findFirstOpenMatch() -> Int {
        if let i = self.viewModel.getData().firstIndex(where: { !$0.isFinished }) {
            return i
        }
        return 0
    }
    
}
