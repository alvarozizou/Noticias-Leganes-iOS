//
//  NewsItemViewModel.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 24/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit

protocol NewsItemViewModel {
    var title:         String {get}
    var link:          URL? {get}
    var origen:        String {get}
    var elapsed:       String {get}
    var origenElapsed: String {get}
    var desc:          String {get}
    var image:         Any? {get}
    var date:          Date {get}
	var dateFormatted: String {get}
}

struct NewsItemViewModelImpl: NewsItemViewModel {
    
    var news : News
    
    var title : String {
        return self.news.title.removingHTMLOccurrences()
    }
    
    var link : URL? {
        return self.news.link
    }
    
    var origen : String {
        guard let origen = self.news.origen else {
            return ""
        }
        
        return origen
    }
    
    var elapsed : String {
        guard let date = self.news.date else {
            return ""
        }
        
        return Date().offset(from: date)
    }
    
    var origenElapsed : String {
        return self.origen + " - " + self.elapsed
    }
    
    var desc : String {
        guard let desc = self.news.desc else {
            return ""
        }
        return desc.removingHTMLOccurrences().addEllipsis()
    }
    
    var image : Any? {
        
        guard self.news.imageUrl != nil else {
            return self.news.defaultImage
        }
        
        return self.news.imageUrl
    }
    
    var date : Date {
        guard let date = self.news.date else {
            return Date()
        }
        
        return date
    }
    
    var dateFormatted : String {
        guard let date = self.news.date else {
            return ""
        }
        
        return date.toString(dateFormat: "EEEE, dd MMMM yyyy")
    }
    
    init(_ news: News) {
        self.news = news
    }
    
}
