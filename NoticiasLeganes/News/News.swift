//
//  Entry.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 15/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
    
public struct News : CustomStringConvertible{
    var title        : String
    var link         : URL?
    var date         : Date?
    var imageUrl     : URL?
    var desc         : String?
    var origen       : String?
    var defaultImage : UIImage?
    
    init(title : String, link : URL?, date : Date?, imageUrl : URL?, description : String?, origen : String?, defaultImage : UIImage?) {
        self.title        = title
        self.link         = link
        self.date         = date
        self.imageUrl     = imageUrl
        self.desc         = description
        self.origen       = origen
        self.defaultImage = defaultImage
        
    }
    
    init(title : String, link : String?, date : Date?, imageUrl : String?, description : String?, origen : String?, defaultImage : UIImage?) {
        
        var linkUrl  : URL?
        var imageUrlAux : URL?
        
        if let _link = link {
            linkUrl = URL(string: _link)
        }
        
        if let _imageUrl = imageUrl {
            imageUrlAux = URL(string: _imageUrl)
        }
        
        self.init(title: title, link: linkUrl, date: date, imageUrl: imageUrlAux, description: description, origen: origen, defaultImage: defaultImage)
        
    }
    
    public var description: String {
        
        var description = "Title: \(self.title)\n"
        description = description + "Description: \(self.desc ?? "")\n"
        description = description + "Link: \(self.link?.absoluteString ?? "")\n"
        description = description + "Image: \(self.imageUrl?.absoluteString ?? "")\n"
        description = description + "Date: \(self.date ?? Date())\n"
        description = description + "Origen: \(self.origen ?? "")\n"
        description = description + "DefaultImage: \(self.defaultImage?.accessibilityIdentifier ?? "")\n"
        
        return description
        
    }
}
