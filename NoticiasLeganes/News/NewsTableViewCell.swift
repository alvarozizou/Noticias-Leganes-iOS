//
//  EntryTableViewCell.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 18/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Kingfisher

class NewsTableViewCell: UITableViewCell {
    
    static let ReuseIdentifier = "NewsTableViewCell"
    
    //MARK: Properties
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var origenLabel: UILabel!
    @IBOutlet weak var elapsedLabel: UILabel!
    
    var newsItemViewModel: NewsItemViewModel? {
        didSet {
            bindViewModel()
        }
    }
    
    private let placeholderNamed = "ic_launcher_bn"
    private let cornerRadiusPhoto:CGFloat = 0
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    private func bindViewModel() {
        if let _newsItemViewModel = newsItemViewModel {
            titleLabel.text   = _newsItemViewModel.title
            origenLabel.text  = _newsItemViewModel.origen
            elapsedLabel.text = _newsItemViewModel.elapsed
        
            let processor = RoundCornerImageProcessor(cornerRadius: cornerRadiusPhoto)
            let placeholder = UIImage(named: placeholderNamed)
        
            photoImageView.setPhotoImageKf(withImage: _newsItemViewModel.image,
                                       placeholder: placeholder,
                                       processor: processor)
        }
    }
}
