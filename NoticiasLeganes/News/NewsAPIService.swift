//
//  NewsAPIService.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 17/9/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit
import FeedKit

class NewsAPIService: DataManager {
    
    func getData(completionHandler: @escaping completion) {
        
        var news = [News]()
        var errorNews: Error?
        
        DispatchQueue.global(qos: .userInitiated).async { // 1
                Feed.getData(completionHandler: { data, error in
                    
                    if error != nil {
                        errorNews = error
                        return
                    }
                    let feeds = data as! [Feed]
                    for var feed in feeds {
                        feed.downloadNews()
                        if let feednews = feed.news {
                            news.append(contentsOf: feednews)
                        }
                    }
                })
            DispatchQueue.main.async {
                if errorNews != nil {
                    completionHandler(nil, errorNews)
                } else {
                    completionHandler(news, nil)
                }
                
            }
        }
    }
}
