//
//  DataManager.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 07/8/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit

protocol DataManager {
    
    typealias completion = (Any?, Error?) -> ()
	func getData(completionHandler: @escaping completion)
    
}
