//
//  TableViewViewModel.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 17/9/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

protocol ListViewModelDelegate : class {
    
    func itemsDidChange()
    func itemsError()

}
