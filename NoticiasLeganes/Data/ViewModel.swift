//
//  ViewModel.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 3/10/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

class ViewModel<T>: NSObject {

    private var apiService : DataManager
    private weak var viewDelegate : ListViewModelDelegate?
    private var data = [T]()
    private var mappingData: (Any?) -> [T]
    
    init(apiService: DataManager, viewDelegate: ListViewModelDelegate, mappingData: @escaping (Any?) -> [T]) {
        self.apiService   = apiService
        self.viewDelegate = viewDelegate
        self.mappingData  = mappingData
    }
    
    func reloadData() {
        //Poner lo de fatalError
        guard let _viewDelegate = self.viewDelegate else {
            fatalError("ViewModel without view delegate")
        }
        apiService.getData(completionHandler: {response, error in
            if error == nil {
                self.data = self.mappingData(response)
                _viewDelegate.itemsDidChange()
            } else {
                _viewDelegate.itemsError()
            }
        })
    }
    
    var count: Int {
        return data.count
    }
    
    func itemAtIndex(_ index: Int) -> T? {
        return data.count > index ? data[index] : nil
    }
    
    func getData() -> [T] {
        return data
    }
    
}
