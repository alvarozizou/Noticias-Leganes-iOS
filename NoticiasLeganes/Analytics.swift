//
//  Analytics.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 21/8/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Crashlytics

struct Analytics {
    
    static func watchNews(_ news: NewsItemViewModel) {
        Answers.logContentView(withName: "Noticia",
                               contentType: news.origen,
                               contentId: news.link?.absoluteString)
    }
    
    static func watchOriginalNews(_ news: NewsItemViewModel) {
        Answers.logContentView(withName: "Noticia Original",
                               contentType: news.origen,
                               contentId: news.link?.absoluteString)
    }
    
    static func shareNews(_ news: NewsItemViewModel) {
        Answers.logShare(withMethod: "Share",
                         contentName: news.title,
                         contentType: news.origen,
                         contentId: news.link?.absoluteString,
                         customAttributes: nil)
    }

    static func watchChat() {
        Answers.logContentView(withName: "Chat",
                               contentType: "Chat",
                               contentId: UserDF.alias)
    }

    static func watchAliasChat() {
        Answers.logContentView(withName: "Chat",
                               contentType: "Alias",
                               contentId: UserDF.alias)
    }

    static func watchPositions() {
        Answers.logContentView(withName: "Positions",
                               contentType: "Positions",
                               contentId: UserDF.alias)
    }

    static func watchMatches() {
        Answers.logContentView(withName: "Matches",
                               contentType: "Matches",
                               contentId: UserDF.alias)
    }

}
