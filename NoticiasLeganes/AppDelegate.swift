//
//  AppDelegate.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 8/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import Firebase
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        Fabric.with([Answers.self, Crashlytics.self])
        FirebaseApp.configure()
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        
        if let nav = window?.rootViewController as? UINavigationController {
            if let tab = nav.viewControllers[0] as? UITabBarController {
                for child in tab.viewControllers ?? [] {
                    switch child {
                    case let top as MatchesViewController:
                        let mappingData: (Any?) -> ([MatchItemViewModel]) = { data in
                            guard let matches = data as? [Match] else {
                                return []
                            }
                            return matches.map{ MatchItemViewModelImpl($0) }
                        }
                        let viewModel = ViewModel<MatchItemViewModel>(apiService: MatchAPIService(), viewDelegate: top, mappingData: mappingData)
                        top.viewModel = viewModel
                        
                    case let top as PositionsViewController:
                        let mappingData: (Any?) -> ([PositionItemViewModel]) = { data in
                            guard let positions = data as? [Position] else {
                                return []
                            }
                            return positions.map{ PositionItemViewModelImpl($0) }
                        }
                        let viewModel = ViewModel<PositionItemViewModel>(apiService: PositionAPIService(), viewDelegate: top, mappingData: mappingData)
                        top.viewModel = viewModel
                        
                    case let top as NewsTableViewController:
                        let mappingData: (Any?) -> ([NewsItemViewModel]) = { data in
                            guard let news = data as? [News] else {
                                return []
                            }
                            var newsItem = news.map{ NewsItemViewModelImpl($0) }
                            newsItem.sort{ $0.date > $1.date }
                            return newsItem
                        }
                        let viewModel = ViewModel<NewsItemViewModel>(apiService: NewsAPIService(), viewDelegate: top, mappingData: mappingData)
                        top.viewModel = viewModel
                    default:
                        print("No View")
                    }
                }
            }
        }


        return true
    }

    func application(_ application: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any])
        -> Bool {
            
            return GIDSignIn.sharedInstance()?.handle(url) ?? true
            
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

}

extension AppDelegate: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error) != nil {
            return
        }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                return
            }
            // User is signed in
            // ...
        }
    }

}


