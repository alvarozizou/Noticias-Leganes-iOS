//
//  NewsTableViewController.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 17/5/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

class NewsTableViewController: UIViewController, ListViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    var refreshControl    = UIRefreshControl()
    
    var viewModel : ViewModel<NewsItemViewModel>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func prepareTableView() {
        prepareRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    @objc func refreshData(_ sender: Any) {
        reloadData()
    }
    
    func reloadData() {
        viewModel.reloadData()
    }
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == K.Segues.news2entry,
            let newsView = segue.destination as? NewsViewController,
            let indexPath = tableView.indexPathForSelectedRow,
            let newsItemViewModel = viewModel.itemAtIndex(indexPath.row)  else {
                fatalError("No segue for show News")
        }
        newsView.newsItemViewModel = newsItemViewModel
    }

}

 extension NewsTableViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: NewsTableViewCell.ReuseIdentifier, for: indexPath) as? NewsTableViewCell  else {
            fatalError("The dequeued cell is not an instance of NewsTableViewCell.")
        }
        
        if let newsItemViewModel = viewModel.itemAtIndex(indexPath.row) {
            cell.newsItemViewModel = newsItemViewModel
        }        
        return cell
        
    }
     
 } 

extension NewsTableViewController: ListViewModelDelegate {
    
    func itemsDidChange() {
        showNewData()
    }
    
    func itemsError() {
        showError()
    }
}
