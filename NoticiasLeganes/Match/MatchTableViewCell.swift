//
//  MatchTableViewCell.swift
//  pruebaAcceso
//
//  Created by Alvaro Blazquez Montero on 23/7/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit

class MatchTableViewCell: UITableViewCell {
    
    static let ReuseIdentifier = "MatchCell"

    @IBOutlet weak var homeTeamLabel:  UILabel!
    @IBOutlet weak var awayTeamLabel:  UILabel!
    @IBOutlet weak var goalsHomeLabel: UILabel!
    @IBOutlet weak var goalsAwayLabel: UILabel!
    @IBOutlet weak var dateLabel:      UILabel!
    
    var matchItemViewModel: MatchItemViewModel? {
        didSet {
            bindViewModel()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func bindViewModel() {
        if let _matchItemViewModel = matchItemViewModel {
            self.homeTeamLabel.text  = _matchItemViewModel.homeTeamName
            self.awayTeamLabel.text  = _matchItemViewModel.awayTeamName
            self.goalsHomeLabel.text = _matchItemViewModel.goalsHomeTeam
            self.goalsAwayLabel.text = _matchItemViewModel.goalsAwayTeam
            self.dateLabel.text      = _matchItemViewModel.date
        }
    }

}
