//
//  MatchAPIService.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 17/9/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class MatchAPIService: DataManager {
    
    func getData(completionHandler: @escaping completion) {
        let headers: HTTPHeaders = [
            "X-Auth-Token": K.Key
        ]
        
        Alamofire.request(K.MatchURL, method: .get, headers: headers).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                let matches = self.getList(json: json)
                completionHandler(matches, nil)
            case .failure(let error):
                completionHandler(nil, error)
            }
        }
    }
    
    private func getList(json: JSON) -> [Match] {
        var matches = [Match]()
        
        let fixtures: Array<JSON> = json[MatchKey.fixtures.rawValue].arrayValue
        for fixture in fixtures {
            let match = Match(json: fixture)
            matches.append(match)
        }
        
        return matches
    }
    
    

}
