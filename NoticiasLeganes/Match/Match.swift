//
//  Match.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 16/6/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit
import SwiftyJSON

enum MatchKey: String {
    case date = "utcDate", status, homeTeam, awayTeam, name
    case goalsHomeTeam, goalsAwayTeam, result = "score", fixtures = "matches", fullTime
}

enum Status {
    case opened
    case finished
}

enum Winner {
    case home
    case away
    case draw
}

struct Match {
    var date          : Date?
    var status        : Status = .opened
    var homeTeamName  : String = ""
    var awayTeamName  : String = ""
    var goalsHomeTeam : Int    = 0
    var goalsAwayTeam : Int    = 0
    var winner: Winner {
        if goalsHomeTeam == goalsAwayTeam {
            return .draw
        }
        return goalsHomeTeam > goalsAwayTeam ? .home : .away
    }
}

extension Match {
    init (json: JSON) {
        self.init()
        self.date          = json[MatchKey.date.rawValue].stringValue
            .toDate(dateFormat: "yyyy-MM-dd'T'HH:mm:ssZ")
        self.status        = json[MatchKey.status.rawValue].stringValue == "FINISHED" ?
            .finished : .opened
        self.homeTeamName  = json[MatchKey.homeTeam.rawValue][MatchKey.name.rawValue].stringValue
        self.awayTeamName  = json[MatchKey.awayTeam.rawValue][MatchKey.name.rawValue].stringValue
        let result = json[MatchKey.result.rawValue][MatchKey.fullTime.rawValue]
        self.goalsHomeTeam = result[MatchKey.homeTeam.rawValue].intValue
        self.goalsAwayTeam = result[MatchKey.awayTeam.rawValue].intValue
    }
}
