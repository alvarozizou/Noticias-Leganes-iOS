//
//  MatchViewModel.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 23/7/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//
import UIKit

protocol MatchItemViewModel {
    var homeTeamName:  String {get}
    var awayTeamName:  String {get}
    var goalsHomeTeam: String {get}
    var goalsAwayTeam: String {get}
    var date:          String {get}
    var isFinished:    Bool {get}
}

struct MatchItemViewModelImpl : MatchItemViewModel {

    var match : Match
    
    var homeTeamName : String {
        return self.match.homeTeamName
    }
    
    var awayTeamName : String {
        return self.match.awayTeamName
    }
    
    var goalsHomeTeam : String {
        return self.match.status == Status.finished ?
                String(self.match.goalsHomeTeam)    :
                ""
    }
    
    var goalsAwayTeam : String {
        return self.match.status == Status.finished ?
                String(self.match.goalsAwayTeam)    :
                ""
    }
    
    var date : String {
        guard let date = self.match.date else {
            return ""
        }
        return date.toString(dateFormat: "dd/MM/yyyy HH:mm")
    }
    
    var isFinished : Bool {
        return self.match.status == Status.finished
    }
    
    init(_ match: Match) {
        self.match = match
    }
    
}
