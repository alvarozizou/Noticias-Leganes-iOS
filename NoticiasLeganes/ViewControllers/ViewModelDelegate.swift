//
//  ViewModelDelegate.swift
//  NoticiasLeganes
//
//  Created by Alvaro Informática on 12/1/18.
//  Copyright © 2018 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit
import Material

struct Process: Equatable {

    var rawValue: String
    static let Main  = Process(rawValue: "Main")

    static func == (lhs: Process, rhs: Process) -> Bool {
        return lhs.rawValue == rhs.rawValue
    }

}

enum ProcessViewState {
    case loading
    case loaded(Process)
    case error(Process)
}

protocol ViewModelDelegate: class {
    func render(state: ProcessViewState)
}

class InputDataViewController: UIViewController, TextFieldDelegate, ViewModelDelegate {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }

    func prepareErrorTextField(placeholder: String, error: String, centerInLayout: UIView?) -> ErrorTextField {
        let errorTextField = ErrorTextField()
        errorTextField.delegate = self

        errorTextField.placeholder = placeholder
        errorTextField.detail = error

        errorTextField.isClearIconButtonEnabled = true
        errorTextField.placeholderActiveColor = UIColor.FlatColor.Blue.Leganes
        errorTextField.dividerActiveColor = UIColor.FlatColor.Blue.Leganes

        centerInLayout?.layout(errorTextField).center().left(0).right(0)

        return errorTextField
    }

    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = false
        return true
    }

    func textField(textField: TextField, didChange text: String?) {
        (textField as? ErrorTextField)?.isErrorRevealed = false
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

    func render(state: ProcessViewState) {

    }
}



