//
//  ListViewController.swift
//  NoticiasLeganes
//
//  Created by Alvaro Blazquez Montero on 7/10/17.
//  Copyright © 2017 Alvaro Blazquez Montero. All rights reserved.
//

import UIKit

protocol ListViewController{
    
    var activityIndicator: UIActivityIndicatorView {get set}
    var refreshControl: UIRefreshControl {get set}
    
    func initTableView()
    
    func prepareTableView()
    func prepareActivityIndicator()
    
    func reloadData()
    func reloadTableView()
    
    func stopAnimation()
    func showError()
    func showNewData()
    
}

extension ListViewController where Self: UIViewController {
    
    func initTableView() {
        prepareActivityIndicator()
        prepareTableView()
        reloadData()
    }
    
    func prepareActivityIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.color = K.mainColor
        activityIndicator.hidesWhenStopped = true
        self.view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
    
    func prepareRefreshControl() {
        refreshControl.tintColor = K.mainColor
    }
    
    func showNewData() {
        stopAnimation()
        reloadTableView()
    }
    
    func stopAnimation() {
        activityIndicator.stopAnimating()
        refreshControl.endRefreshing()
    }
    
    func showError() {
        stopAnimation()
        
        let alertController = UIAlertController(title: "¡Vaya!", message: "Parece que hemos tenido un problema. Vuelve a intentarlo.", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title: "Vuelve a intentarlo", style: UIAlertAction.Style.default) {
            (result : UIAlertAction) -> Void in
            self.reloadData()
        }
        
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
